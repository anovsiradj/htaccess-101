<?php

$base = dirname($_SERVER['SCRIPT_NAME']); // HARUS

$title = basename(__DIR__);
?>

<link rel="stylesheet" href="<?= $base ?>/assets/theme.css">
<title><?= $title ?></title>

<h1><a href="<?= "$base/index" ?>">/index</a></h1>
<h1><a href="<?= "$base/test" ?>">/test</a></h1>
<h1><a href="<?= "$base/blog" ?>">/blog</a></h1>
<h1><a href="<?= "$base/page" ?>">/page</a></h1>
<h1>
	<a href="<?= "$base/user" ?>">/user</a>
	<a href="<?= "$base/user/view/yuhan" ?>">/user/view/yuhan</a>
</h1>
<h1>
	<a href="<?= "$base/fake" ?>">/fake</a>
	<a href="<?= "$base/real" ?>">/real</a>
</h1>
<h1>
	<a href="<?= "$base/alias-lama.php" ?>">/alias-lama.php</a>
	<a href="<?= "$base/alias-baru.php" ?>">/alias-baru.php</a>
</h1>
