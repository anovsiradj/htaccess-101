<?php

$base = dirname($_SERVER['SCRIPT_NAME']); // HARUS

$blogs = [
	5 => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
	19 => 'Unde molestias eligendi animi deleniti tenetur vero quae dolore reiciendis cumque.',
	22 => 'Laudantium quas sapiente provident minus magni, excepturi velit officiis similique, pariatur.',
];

$blogId = !empty($_GET['id']) ? $_GET['id'] : null;

$title = $blogId ? 'Blog Item' : 'Blog List';
?>

<link rel="stylesheet" href="<?= $base ?>/assets/theme.css">

<h1><?= $title ?></h1>

<?php if ($blogId) { ?>
	<?= $blogs[$blogId] ?>
	<h2><a href="<?= "$base/blog" ?>">kembali</a></h2>
<?php } else { ?>
	<ul>
		<?php foreach ($blogs as $id => $blog) { ?>
			<li>
				<a href="<?= "$base/blog/$id" ?>"><?= $blog ?></a>
			</li>
		<?php } ?>
	</ul>
<?php } ?>
