<?php

$base = dirname($_SERVER['SCRIPT_NAME']); // HARUS

$pageSlug = !empty($_GET['slug']) ? $_GET['slug'] : 'home';

$title = 'page';
?>

<link rel="stylesheet" href="<?= $base ?>/assets/theme.css">

<a href="<?= "$base/page" ?>">home</a>
<a href="<?= "$base/page/about" ?>">about</a>
<a href="<?= "$base/page/contact" ?>">contact</a>

<?php require __DIR__ . "/pages/$pageSlug.php" ?>
